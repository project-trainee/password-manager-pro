create or replace procedure findmaxvalue(tablename text)
AS
$$
DECLARE 
rownum int;
columnum int;
tablenametext text;
maxcolumntext text;
columnlist text[];
start int:=1;
BEGIN
	tablenametext:='select count(*) from '||tablename||';';
	execute tablenametext into rownum;
	SELECT count(*) into columnum  FROM information_schema.columns WHERE table_name =tablename;
	select array(select column_name::text from information_schema.columns where table_name = tablename) into columnlist;

	while start<=rownum loop
		raise notice '% present in row %',maximumvalue(start,columnum,columnlist,tablename),start;
		start:=start+1;

	end loop;
end;
$$
language plpgsql;






create or replace function maximumvalue(rownum integer,columnum integer,columnlist text[],tablename text)
returns integer AS
$$

DECLARE 
value1 integer;
columndatatype text;
columndatatypecheck text;
startcolumn integer:=1;
maxval integer:=0;
columnnames varchar(200);
BEGIN
		while startcolumn<=columnum loop
		columndatatypecheck:='SELECT pg_typeof('||columnlist[startcolumn]||')  FROM '||tablename ||' LIMIT 1;';
		execute columndatatypecheck into columndatatype;
		if columndatatype='integer' then
			columnnames:='SELECT '||columnlist[startcolumn]||' from '||tablename||' limit 1 offset '||rownum-1||';';
			execute columnnames into value1;
			if value1>maxval then
				maxval:=value1;
			end if;
		end if;
		startcolumn:=startcolumn+1;
	end loop;
	return maxval;
end;

$$
language plpgsql;


call findmaxvalue('marklist');
